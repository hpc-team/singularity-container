singularity-container (4.1.5+ds4-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 4.1.5+ds4 (Repacked containerd)
    Closes: #1087556

 -- Nilesh Patra <nilesh@debian.org>  Fri, 28 Feb 2025 00:39:54 +0530

singularity-container (4.1.5+ds3-1) unstable; urgency=medium

  * Team Upload.
  * Restore .gitlab-ci.yml and prevent it from getting deleted
  * New upstream version 4.1.5+ds3
  * Update minimum version of patternmatcher-dev
  * Update dependency on golang-lru
  * Update dependency on image-spec
  * Pre-repack change to vendor x/sys
  * Update copyright

 -- Nilesh Patra <nilesh@debian.org>  Tue, 20 Aug 2024 00:16:45 +0530

singularity-container (4.1.5+ds2-1) unstable; urgency=medium

  * Team Upload.
  * Update vendored libs to exclude and copyright licenses
  * New upstream version 4.1.5+ds1 (Closes: #1078515)
    + Update paths (Drop jose.v2 patch -- no longer used)
  * Add new Build-Deps
  * Update GO_TEST_TAGS
  * Update test to check for regression with #1012469

 -- Nilesh Patra <nilesh@debian.org>  Sun, 18 Aug 2024 22:42:30 +0530

singularity-container (4.1.2+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 4.1.2+ds1
  * Add Builddep on golang-github-felixge-httpsnoop-dev

 -- Nilesh Patra <nilesh@iki.fi>  Sun, 07 Apr 2024 16:55:43 +0530

singularity-container (4.1.1+ds2-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 4.1.1+ds1
  * Exclude a bunch of vendored stuff, add corresponding B-D
    (Heavy refactoring)
  * Drop conveyorPacker_oci.patch, refresh rest patches
  * Remove copyright for removed vendored dirs
  * Remove Yaroslav and onlyjob from uploaders. Thanks for your work.

 -- Nilesh Patra <nilesh@debian.org>  Sun, 25 Feb 2024 03:42:28 +0530

singularity-container (4.0.3+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 4.0.3+ds1
  * d/control:
    + Add B-D on golang-github-go-jose-go-jose-dev
  * d/copyright:
    + Exclude the following vendored packages:
      - vendor/github.com/go-jose/go-jose/v3
      - vendor/github.com/samber/lo

 -- Nilesh Patra <nilesh@debian.org>  Tue, 16 Jan 2024 20:19:18 +0530

singularity-container (4.0.2+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 4.0.2+ds1
  * d/control:
    + Add versioned B-D on a container image related deps
    + Add vesioned B-D on golang-any
    + Add versioned B-D on golang-github-vbauerster-mpb-dev
  * d/copyright:
    + Remove following packages from exclusion which are
      not vendored anymore:
      - vendor/github.com/theupdateframework/go-tuf
    + Exclude the following vendored stuff:
      - vendor/github.com/vbauerster/mpb

 -- Nilesh Patra <nilesh@debian.org>  Mon, 20 Nov 2023 04:35:21 +0530

singularity-container (4.0.1+ds1-1) unstable; urgency=medium

  [ Nilesh Patra ]
  * Team Upload.
  * New upstream version 4.0.1+ds1 (Refresh patches)
  * d/copyright:
    + Remove following packages from exclusion which are
      not vendored anymore:
      - vendor/github.com/go-logr/logr
      - vendor/github.com/go-logr/stdr
      - vendor/github.com/kr/pty
      - vendor/github.com/moby/locker
      - vendor/go.etcd.io/bbolt
      - vendor/gopkg.in/square/go-jose.v2
      - vendor/go.opentelemetry.io/otel
      - vendor/oras.land/oras-go
      - third_party/squashfuse
    + Exclude following vendored stuff:
      - vendor/github.com/containerd/stargz-snapshotter
      - vendor/github.com/mattn/go-sqlite3
      - vendor/github.com/mitchellh/go-homedir
    + Include back following modules due to FTBFS with system packages
      - vendor/github.com/container-orchestrated-devices/container-device-interface
    + Add copyrights for the new included modules.
    + Mention myself in copyright, update copyright year
  * d/control:
    + Add new B-D on:
      - golang-github-containerd-stargz-snapshotter-dev
      - golang-github-mitchellh-go-homedir-dev
      - golang-github-samber-lo-dev
    + Remove B-D on:
      - golang-github-coreos-bbolt-dev
      - golang-github-kr-pty-dev
      - golang-github-moby-locker-dev
      - golang-github-morikuni-aec-dev
      - golang-oras-oras-go-dev
    + Add Recommends on:
      - squashfuse
    + Remove duplicate B-D on golang-github-containers-image-dev
  * d/rules:
    + Pass --without-squashfuse to use system provided binary
    + Add pkg/ocibundle/ocisif to DH_GOLANG_EXCLUDES for tests
    + Run tests with package specific tags GO tags
      It otherwise FTBFS since it needs at least singularity_enigne tag
    + Fix perms of verify_ocsp.go, remove +x perms
  * Add lintian override for field-too-long Built-Using (Heavy package)

  [ Chirag Sukhala ]
  * Added basic unit test case that could be used to validate functionality
    befiore each upload. For now this needs to be run manually as singularity
    can not run inside lxc container used at debci.

 -- Nilesh Patra <nilesh@debian.org>  Wed, 18 Oct 2023 23:45:24 +0530

singularity-container (3.11.4+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.11.4+ds1
  * d/copyright:
   + Remove following packages from exclusion which are
     not vendored anymore:
     - vendor/github.com/containerd/cgroups
     - vendor/github.com/golang/groupcache
     - vendor/github.com/Microsoft/hcsshim
   + Exclude following vendored stuff:
     - vendor/github.com/go-logr/logr
     - vendor/github.com/go-logr/stdr
     - vendor/github.com/moby/patternmatcher
     - vendor/go.opentelemetry.io/otel
     - vendor/gopkg.in/go-jose/go-jose.v2
     - vendor/golang.org/x/exp
     - vendor/gopkg.in/yaml.v3
    + Include back following modules due to FTBFS with
      system packages:
     - vendor/github.com/docker/docker
     - vendor/github.com/moby/sys
     - vendor/github.com/vbauerster/mpb
    + Add copyrights for the new included modules.
  * d/control:
    + Change B-D and dep on pelletier-go-toml to go-toml.v2
    + Add B-D on moby-patternmatcher abd yaml.v3

 -- Nilesh Patra <nilesh@debian.org>  Tue, 01 Aug 2023 18:48:10 +0530

singularity-container (3.11.0+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.11.0+ds1, Re-diff patches
    + Addresses CVE-2022-23538
  * Exclude more vendor files, add required B-D
  * Bump Standards-Version to 4.6.2 (no changes needed)
  * Add patch to ignore build time test that needs network access
  * Remove couple of vendor symlinks to avoid weird build issues

 -- Nilesh Patra <nilesh@debian.org>  Sat, 04 Mar 2023 00:42:38 +0530

singularity-container (3.10.3+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.10.3+ds1 (addresses CVE-2022-39237)
  * De-vendor vendor/github.com/cloudflare/circl, add B-D on corresponding
    package in the archive
  * Update copyr as per new vendor packages
  * Fix d/watch, Re-diff patches
  * Add patch to adjust vbauerster-mpb as per version in archive

 -- Nilesh Patra <nilesh@debian.org>  Wed, 12 Oct 2022 20:07:12 +0530

singularity-container (3.10.2+ds3-1) unstable; urgency=medium

  * Team Upload.
  * Prune rootless-containers/proto out of vendor dir
    (Closes: #1020208)
  * Fix d/watch

 -- Nilesh Patra <nilesh@debian.org>  Sun, 25 Sep 2022 13:44:41 +0530

singularity-container (3.10.2+ds2-3) unstable; urgency=medium

  * Team upload.
  * Stop B-D on libpod-dev thus saving singularity
    from unrelated auto-RMs

 -- Nilesh Patra <nilesh@debian.org>  Fri, 05 Aug 2022 00:35:38 +0530

singularity-container (3.10.2+ds2-2) unstable; urgency=medium

  * Team upload.
  * Drop golint from B-D (Closes: #1015199)

 -- Nilesh Patra <nilesh@debian.org>  Sun, 31 Jul 2022 09:32:01 +0000

singularity-container (3.10.2+ds2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.10.2+ds2
  * d/rules:
    + Do not install testdata, it is useless for end-user anyway
    + Enable --without-conmon option for mconfig, allow it to
      use system binary.
    + Remove refs to third_party
    + Propagate upstream version without repackaging suffix
  * d/control:
    + Change section of -dev package to "golang"
    + Add Recommends on conmon and runc
  * d/patches:
    + Update conveyorPacker_oci.patch
    + Drop change-libpod-path.patch
  * d/copyright:
    + Update copyright, mention more files
    + Prune out vendored deps and files that could be non-free
  * d/u/metadata: Use "Repository" field instead of "Homepage" field
  * Update lintian override on elevated-permission

 -- Nilesh Patra <nilesh@debian.org>  Sun, 31 Jul 2022 08:22:01 +0000

singularity-container (3.10.1+ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.10.1+ds1
  * Drop fix-mipsel-ftbfs.patch: Merged upstream

 -- Nilesh Patra <nilesh@debian.org>  Sun, 24 Jul 2022 15:53:40 +0530

singularity-container (3.10.0+ds2-3) unstable; urgency=medium

  * Team upload.
  * Modify oci patch to return right imgspec
    (Closes: #1012469)
  * Add desc to patches
  * d/u/metadata: Make yamllint happy

 -- Nilesh Patra <nilesh@debian.org>  Fri, 17 Jun 2022 11:03:52 +0530

singularity-container (3.10.0+ds2-2) unstable; urgency=medium

  * Team upload.
  * Add patch to attempt fixing mipsel FTBFS

 -- Nilesh Patra <nilesh@debian.org>  Tue, 14 Jun 2022 09:11:33 +0530

singularity-container (3.10.0+ds2-1) unstable; urgency=medium

  * Team upload.
  * Remove bad pre-compiled files
  * New upstream version 3.10.0+ds2

 -- Nilesh Patra <nilesh@debian.org>  Tue, 14 Jun 2022 04:34:00 +0530

singularity-container (3.10.0+ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.10.0+ds1
  * Exclude more vendor and third_party files
  * d/control: Add new B-D
  * d/rules:
    + Remove containers/storage before build
    since this somehow linked twice leading to gcc-based
    fno-common failures
    + Copy third_party/conmon dir inside vendor
  * d/clean: Clean copied third_party/conmon inside vendor dir
  * d/patches:
    + Add change-libpod-path.patch: Change libpod
      import path in acc with the package in archive
    + Refresh patches
    + Drop bump_further_mbp_from_v4_to_v7.patch

 -- Nilesh Patra <nilesh@debian.org>  Mon, 13 Jun 2022 19:01:00 +0000

singularity-container (3.9.9+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.9.9+ds1

 -- Nilesh Patra <nilesh@debian.org>  Wed, 27 Apr 2022 21:04:31 +0530

singularity-container (3.9.8+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.9.8+ds1
  * d/{copyright,control}: golang-github-j-keck-arping-dev is not
    needed anymore, remove refs to it

 -- Nilesh Patra <nilesh@debian.org>  Fri, 15 Apr 2022 22:45:45 +0530

singularity-container (3.9.6+ds1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.9.6+ds1
  * Remove vendored mvdan.cc/sh and
    oras.land/oras-go, use packaged ones
  * Update bump_further_mbp_from_v4_to_v7.patch
  * Drop d052a709acbe1fc2eae989842e4248a5ff90a591.patch
  * d/.gitlab-ci.yml: Disable reprotest and autopkgtest

 -- Nilesh Patra <nilesh@debian.org>  Thu, 17 Mar 2022 00:43:41 +0530

singularity-container (3.9.5+ds1-3) unstable; urgency=medium

  * Team Upload.
  * Add patch to fix build on mips

 -- Nilesh Patra <nilesh@debian.org>  Thu, 24 Feb 2022 12:20:27 +0530

singularity-container (3.9.5+ds1-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable

 -- Andreas Tille <tille@debian.org>  Wed, 23 Feb 2022 17:41:49 +0100

singularity-container (3.9.5+ds1-1) experimental; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * Version > 3.6.x are closing CVE-2021-33622
    Closes: #990201
  * Add debian/README.source with my findings about issues when trying
    to replace vendored code copies
  * Remove superfluous file patterns from d/copyright
  * Remove unneeded patches
  * Cleanup list of Files-Excluded
  * Drop Afif Elghraoui from Uploaders (thank you for your work Afif)

  [ Nilesh Patra ]
  * d/copyright: Exclude more vendor copies
  * New upstream version 3.9.5+ds1
  * d/control: Add/update B-D for dropped vendor copies
  * Adapt patch to newer release
  * Drop d/README.source

 -- Nilesh Patra <nilesh@debian.org>  Sun, 20 Feb 2022 19:27:46 +0530

singularity-container (3.9.4+ds2-1) experimental; urgency=medium

  * Team upload

  [ Yaroslav Halchenko ]
  * Fresh upstream release
    Closes: #995171
  * debian/watch - updated to point to -ce flavor of release on github.
    Thanks to Andreas Tille for the patch!
  * debian/patches
    - cni_plugin_path.patch - updated for moved files and more files with
      similar references of cni

  [ Andreas Tille ]
  * New upstream release
     - Version 3.6.0 addresses CVE-2020-13845 CVE-2020-13846 CVE-2020-13847
       Closes: #965040
     - Version 3.6.3 addresses CVE-2020-25039 CVE-2020-25040
       Closes: #970465
     - Version 3.6.4 addresses CVE-2020-15229
       Closes: #972212
  * Versioned (Build-)Depends: golang-github-blang-semver-dev (>= 4)
  * Versioned Build-Depends: golang-github-vbauerster-mpb-dev (>= 7.3.2)
  * Standards-Version: 4.6.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Use secure URI in Homepage field.
  * Update renamed lintian tag names in lintian overrides.
  * Use default salsa-ci pipeline
  * Bump github.com/vbauerster/mpb to v7
  * Drop Dave Love from Uploaders (Thanks a lot for your initial work Dave)
  * Versioned Build-Depends: golang-github-appc-cni-dev (>= 1.0.1~)
  * Do not try to `chown -c root.root` which is not permitted and granted
    in the final package anyway
  * Fix and simplify watch file
  * Replace vendored copies of
     github.com/Azure/go-ansiterm (golang-github-azure-go-ansiterm-dev)
     github.com/Netflix/go-expect (golang-github-netflix-go-expect-dev)
     github.com/ProtonMail/go-crypto (golang-github-protonmail-go-crypto-dev)
  * Distribute Apache-2.0 copyright NOTICE

  [ Nilesh Patra ]
  * Follow pattern of d052a709acbe1fc2eae989842e4248a5ff90a591.patch to remove
    refervences to v4

 -- Andreas Tille <tille@debian.org>  Fri, 18 Feb 2022 17:49:26 +0100

singularity-container (3.5.2+ds2-1) unstable; urgency=medium

  * Repack the source tarball to remove vendored containers/image.
    * Fix FTBFS by backporting upstream patches. (Closes: #964779)
  * Append myself to the Uploaders.
  * Add dependency on golang-github-kr-pty-dev. (Closes: #978441)
  * Drop source-contains-empty-directory lintian tag.

 -- Benda Xu <orv@debian.org>  Mon, 20 Dec 2021 21:03:39 +0800

singularity-container (3.5.2+ds1-1) unstable; urgency=high

  * New upstream release.
    + CVE-2019-19724: fixed insecure permissions on "~/.singularity".

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 18 Dec 2019 08:03:19 +1100

singularity-container (3.5.1+ds1-1) unstable; urgency=medium

  * New upstream release.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 06 Dec 2019 09:45:24 +1100

singularity-container (3.5.0+ds1-2) unstable; urgency=medium

  * Added basic upstream metadata.
  * New upstream patches to fix FTBFS on ppc64el.

 -- Dmitry Smirnov <onlyjob@debian.org>  Sun, 01 Dec 2019 16:37:55 +1100

singularity-container (3.5.0+ds1-1) unstable; urgency=medium

  * New upstream release.
  * Added README.Debian with --fakeroot troubleshooting instructions.
  * Use CNI plugins provided by "containernetworking-plugins".
  * Un-vendored "github.com/creack/pty".

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 14 Nov 2019 16:50:23 +1100

singularity-container (3.4.2+ds2-4) unstable; urgency=medium

  [ Arnaud Rebillout ]
  * New patch to fix FTBFS with Docker 19.03.4.
  * Build-Depends:
    = golang-github-docker-docker-dev (>= 19.03.4~)

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 06 Nov 2019 08:35:47 +1100

singularity-container (3.4.2+ds2-3) unstable; urgency=medium

  * Verbose build.
  * Set "GO111MODULE=off" to prevent build-time download.
  * Removed useless vendored "Microsoft/hcsshim".
  * Build-Depends:
    - golang-github-containerd-continuity-dev
    + golang-github-apex-log-dev

    Thanks, Arnaud Rebillout.

 -- Dmitry Smirnov <onlyjob@debian.org>  Fri, 01 Nov 2019 08:02:13 +1100

singularity-container (3.4.2+ds1-2) unstable; urgency=medium

  * Upload to unstable.
  * Un-vendored more libraries.
  * Build-Depends:
    - golang-github-kelseyhightower-envconfig-dev
    - golang-github-opencontainers-runc-dev
    + golang-github-appc-cni-dev (>= 0.7.1~)
    + golang-github-containernetworking-plugins-dev
    + golang-github-sylabs-json-resp-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 24 Oct 2019 14:20:23 +1100

singularity-container (3.4.2+ds-1) experimental; urgency=medium

  * New upstream release.
  * Build-Depends:
    - golang-github-magiconair-properties-dev
    + cryptsetup-bin
    + golang-github-opensuse-umoci-dev
  * dev: sub-vendor bundled dependencies.

 -- Dmitry Smirnov <onlyjob@debian.org>  Wed, 16 Oct 2019 10:29:57 +1100

singularity-container (3.3.0+ds-2) experimental; urgency=medium

  * Introduced -dev package.

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 10 Oct 2019 14:56:26 +1100

singularity-container (3.3.0+ds-1) experimental; urgency=medium

  * New upstream release.
  * Minor copyright corrections.
  * watch file format to version 4.
  * Un-vendored packaged libraries:
    - golang-github-pquerna-ffjson-dev
    - golang-github-containers-storage-dev
    - golang-github-safchain-ethtool-dev
  * Don't use custom repack; explicit exclusion through standard
    copyright/Files-Excluded field.
  * Drop Files-Excluded from "override_dh_clean".
  * Run some upstream tests.
  * rules to use dh_golang more (no longer use stow).
  * Increased build verbosity.
  * Standards-Version: 4.4.1.
  * DH & compat to version 12.
  * Build-Depends:
    = golang-github-docker-docker-dev (>= 18.09.9~)
    + golang-github-blang-semver-dev
    + golang-github-buger-jsonparser-dev
    + golang-github-containerd-continuity-dev
    + golang-github-containers-storage-dev
    + golang-github-hashicorp-go-retryablehttp-dev (>= 0.6.2~)
    + golang-github-kelseyhightower-envconfig-dev
    + golang-github-pquerna-ffjson-dev
    + golang-github-safchain-ethtool-dev
    + libgpgme-dev

 -- Dmitry Smirnov <onlyjob@debian.org>  Thu, 10 Oct 2019 14:22:53 +1100

singularity-container (3.1.1+ds-1) unstable; urgency=medium

  * Upstream version 3.1.1+ds
  * drop d/patches/ftbfs_mips
    (was ineffectual previously and no longer applies)
  * Incorporate upstream patch for CVE-2019-11328

 -- Afif Elghraoui <afif@debian.org>  Wed, 15 May 2019 02:04:21 -0400

singularity-container (3.0.3+ds-1) unstable; urgency=medium

  * New upstream version; code base rewritten in Go
  * New packaging to handle changes to the language and build system
    - Updated d/patches
    - Created a repack script to remove all but certain specific bundled
      dependencies when importing the source tarball.
  * Updated URLs and d/copyright for singularityware -> sylabs ownership
  * Standards-Version 4.3.0
  * Used canonical VCS URLs

 -- Afif Elghraoui <afif@debian.org>  Wed, 30 Jan 2019 22:14:04 -0500

singularity-container (2.6.1-2) unstable; urgency=medium

  * d/patches/reproducible_build: escape dollar signs in Makefile patch
    (Closes: #919026)

 -- Afif Elghraoui <afif@debian.org>  Mon, 14 Jan 2019 21:23:04 -0500

singularity-container (2.6.1-1) unstable; urgency=high

  [ Yaroslav Halchenko ]
  * d/rules
    + DEB_LDFLAGS_MAINT_STRIP=-Wl,-Bsymbolic-functions
      to prevent a weird side-effect on Ubuntu using those LDFLAGS
      and causing static variable to become having multiple "instances".
      Ref: https://github.com/singularityware/singularity/issues/1947

  [ Afif Elghraoui ]
  * New upstream security release fixing CVE-2018-19295

 -- Afif Elghraoui <afif@debian.org>  Thu, 13 Dec 2018 01:47:28 -0500

singularity-container (2.5.2-2) unstable; urgency=medium

  * d/control
    + depend on ca-certificates since required to pull images from shub://

 -- Yaroslav Halchenko <debian@onerussian.com>  Tue, 10 Jul 2018 14:41:56 -0400

singularity-container (2.5.2-1) unstable; urgency=high

  * New upstream security and bug-fix release

 -- Afif Elghraoui <afif@debian.org>  Wed, 04 Jul 2018 15:06:29 -0400

singularity-container (2.5.1-1) unstable; urgency=high

  [ Yaroslav Halchenko ]
  * debian/control
    - Homepage moved to www.sylabs.io

  [ Afif Elghraoui ]
  * New upstream bug-fix release
  * Refresh patches

 -- Afif Elghraoui <afif@debian.org>  Thu, 03 May 2018 22:12:31 -0400

singularity-container (2.5.0-1) unstable; urgency=high

  * New upstream version, including security updates
  * Add build-dependency on libarchive
  * Remove patch merged upstream

 -- Afif Elghraoui <afif@debian.org>  Mon, 30 Apr 2018 23:04:24 -0400

singularity-container (2.4.6-2) unstable; urgency=medium

  * debian/patches/1461.patch to resolve inability to use image
    with a long filename
    (ref: https://github.com/singularityware/singularity/issues/1185)

 -- Yaroslav Halchenko <debian@onerussian.com>  Sun, 15 Apr 2018 23:43:11 -0400

singularity-container (2.4.6-1) unstable; urgency=medium

  * New upstream release
  * debian/patches/reproducible_build
    - do not provide --sort=name if the available version of tar does not
      support it

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 12 Apr 2018 16:05:31 -0400

singularity-container (2.4.5-1) unstable; urgency=medium

  [ Yaroslav Halchenko ]
  * Revert 01fd7b1b0ab23b03b424e567342dec180b45e2d7 to bring back
    compatibility with debhelper 9 to ease backports (Closes: #893133)

  [ Afif Elghraoui ]
  * New upstream version

 -- Afif Elghraoui <afif@debian.org>  Sat, 24 Mar 2018 16:24:45 -0400

singularity-container (2.4.4-1) unstable; urgency=medium

  * New upstream version
  * Refresh patches

 -- Afif Elghraoui <afif@debian.org>  Sat, 10 Mar 2018 16:15:26 -0500

singularity-container (2.4.2-4) unstable; urgency=low

  * Standards-Version 4.1.3
  * debhelper compat 11
  * Change squashfs tools to Depends from Recommends
  * Recommend e2fsprogs (Closes: #887192)
  * d/rules: add comment explaining why test suite can't be run

 -- Afif Elghraoui <afif@debian.org>  Sat, 17 Feb 2018 17:14:21 -0500

singularity-container (2.4.2-3) unstable; urgency=low

  * Add Recommends: squashfs-tools
  * lintian overrides: update suid binary locations

 -- Afif Elghraoui <afif@debian.org>  Sun, 04 Feb 2018 20:04:43 -0500

singularity-container (2.4.2-2) unstable; urgency=medium

  * Make package build reproducible again by making sure content of
    $HOME doesn't end up in manpages.
  * Fix FTBFS on mips* architectures (Closes: #883466). Thanks to
    James Cowgill for the patch!
  * Set "Debian HPC team" in the maintainer field.
  * Migrate Git repo on Salsa and update Vcs-* fields accordingly
  * Run wrap-and-sort

 -- Mehdi Dogguy <mehdi@debian.org>  Sun, 07 Jan 2018 20:56:09 +0100

singularity-container (2.4.2-1) unstable; urgency=medium

  * New upstream release
    - Fixed an issue for support of older distributions and kernels
      with regards to the setns() function
    - Fixed autofs bug path
  * Get source package's version using /usr/share/dpkg/pkg-info.mk
    instead of calling dpkg-parsechangelog in debian/rules. This is
    possible since dpkg >= 1.16.1.

 -- Mehdi Dogguy <mehdi@debian.org>  Sat, 06 Jan 2018 20:27:48 +0100

singularity-container (2.4.1-1) unstable; urgency=medium

  * Upload to unstable: general functionality is proven to be robust
    and 2.4.x will be needed for upcoming htcondor 8.6.8 with singularity
    support

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 30 Nov 2017 14:46:30 -0500

singularity-container (2.4.1-1~exp1) experimental; urgency=medium

  * New upstream release
    - Fixed container path and owner limitations
    - Abort if overlay upper/work images are symlinks
    - Addition of APP[app]_[LABELS,ENV,RUNSCRIPT,META] so apps can
      internally find one another
    - Exposing labels for SCI-F in environment
  * debian/patches/reproducible_build
    - Refreshed patch

 -- Mehdi Dogguy <mehdi@debian.org>  Mon, 27 Nov 2017 21:22:42 +0100

singularity-container (2.4-1) experimental; urgency=medium

  * New upstream release
    - atomic 'build' process (no need to pre-create an image)
      - create command is deprecated in favor of image.create
    - new default image format based on SquashFS
    - more info: http://singularity.lbl.gov/release-2-4
  * debian/control
    - fixed Homepage (thanks Afif Elghraoui) (Closes: #850227)
  * debian/patches/reproducible_build
    - thanks tiress Chris Lamb (Closes: #866169)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 12 Oct 2017 21:55:26 -0400

singularity-container (2.3.2-1) unstable; urgency=medium

  * New upstream release
    - debian/patches/up_makedirs_cache is no longer pertinent

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 07 Oct 2017 00:15:10 -0400

singularity-container (2.3.1-2) unstable; urgency=medium

  * Provide version for help2man from this changelog (Closes: #871371)

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 07 Aug 2017 19:33:57 -0400

singularity-container (2.3.1-1) unstable; urgency=high

  * Fresh upstream minor release
    - A potential escalation pathway was identified that could have
    allowed a malicious user to escalate their privileges on hosts that do not
    support the PR_SET_NO_NEW_PRIVS flag for the prctl() system call. This
    release fixes this as well as several other identified bugs and potential
    race conditions.

 -- Yaroslav Halchenko <debian@onerussian.com>  Mon, 26 Jun 2017 16:04:51 -0700

singularity-container (2.3-1) unstable; urgency=medium

  * Fresh upstream minor release
  * debian/patches -- all dropped (upstreamed)
  * debian/copyright -- updated copyright holders/years
  * debian/rules
    - a few path tune ups for permissions tuning as introduced upstream
    - apparently still uses /var as default for localstatedir, so specified
      /var/lib explicitly
    - export SINGULARITY_CACHEDIR env var to instruct singularity
      to not attempt create cachedir somewhere under /nonexistent path
      Also added a patch to not fail to create that cache directory if
      parent doesn't exist
  * debian/control
    - Python to build-depends

 -- Yaroslav Halchenko <debian@onerussian.com>  Fri, 02 Jun 2017 22:56:30 -0400

singularity-container (2.2.1-1) experimental; urgency=medium

  * Fresh upstream bugfix release (includes all included in 2.2-2 changesets,
    which are dropped now here)

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 15 Feb 2017 10:42:39 -0500

singularity-container (2.2-2) unstable; urgency=high

  * debian/patches - picks up from upcoming 2.2.1 release
    critical functionality and possibly security-related fixes
    - changeset_b859cd8b4b9293f2a8a893ef41c5d93a5318dd6c.diff
      to support mounting ext4 formatted images read-only
    - changeset_f79e853d9ee8a15b1d16cdc7dfbe85eca50efc6d.diff
      to utilize mount option MS_NODEV for images
      (fixes potential security implications)
    - changeset_d835fa1d20efc4aaacca4be68431d193d6625bd8.diff
      to fix bootstrapping ran as root (thus no MS_NODEV restriction
      from above patch should be applied)
    - changeset_3a2b6537f0b1386336e29d7f763ae62374a7cb77.diff
      exit with error if snprintf would have went out of bounds
    - changeset_acc02b921192e7e16afe1513d5338904f8e6f907.diff
      changeset_0935d68145ce575444b7ced43417cc6fccffd670.diff
      changeset_0d04edaeb5cb3607ab25588f4db177c0878adcc0.diff
      Various obvious fixes (updated URLs, apt --force-yes)

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 09 Feb 2017 16:27:55 -0500

singularity-container (2.2-1) unstable; urgency=medium

  [ Mehdi Dogguy ]
  * Team upload.
  * New upstream release.
    - Install sexec-suid utility instead of sexec
  * Add a override_dh_auto_test in debian/rules to skip testsuite which
    is not well-suited for auto-builders.

  [ Yaroslav Halchenko ]
  * debian/control
    - Vcs- fields adjusted to point to https://anonscm.debian.org/git
    - Mehdi to uploaders
    - Depends on python since some python scripts are used
  * Enforce world readable permissions for /var/singularity{,/mnt{,/source}}
    directories (needed since 2.2)
  * debian/patches
    - 000*-*py* - to not install not to be executed python modules as scripts
    - 000*-bash* - bash completions script has bashisms
  * debian/rules
    - bash completion script under /usr/share/bash-completion/completions
  * use /var/lib/singularity (instead of FHS-noncompliant /var/singularity)
    Next upstream release will adhere to this location as well.
    Previously used location /var/singularity is left untouched -- inspect
    and cleanup.

 -- Yaroslav Halchenko <debian@onerussian.com>  Wed, 30 Nov 2016 12:33:01 -0500

singularity-container (2.1.2-1) unstable; urgency=medium

  * Fresh upstream release

 -- Yaroslav Halchenko <debian@onerussian.com>  Sat, 06 Aug 2016 14:13:44 -0400

singularity-container (2.1~testing0+git39-g875d469-1) unstable; urgency=medium

  * Fresh pre-release snapshot
  * Upload to Debian proper (Closes: #828970)
  * debian/control
    - Place under NeuroDebian team maintenance
  * debian/rules
    - Generate quick and dirty manpages using help2man

 -- Yaroslav Halchenko <debian@onerussian.com>  Thu, 14 Jul 2016 11:03:15 -0400

singularity-container (2.0-1) UNRELEASED; urgency=medium

  * New upstream release
  * Initial Debian packaging

 -- Dave Love <fx@gnu.org>  Thu, 02 Jun 2016 22:48:28 +0100

singularity-container (1.0-1) UNRELEASED; urgency=low

  * Initial release

 -- Dave Love <fx@gnu.org>  Sun, 17 Apr 2016 12:22:41 +0100
