Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: singularity
Upstream-Contact: Gregory M. Kurtzer <gmkurtzer@lbl.gov>
Source: https://github.com/sylabs/singularity
Files-Excluded:
    examples/legacy
    third_party/conmon
    third_party/squashfuse
    vendor/github.com/acarl005/stripansi
    vendor/github.com/alexflint/go-filemutex
    vendor/github.com/apex/log
    vendor/github.com/Azure
    vendor/github.com/beorn7/perks
    vendor/github.com/blang/semver
    vendor/github.com/buger/jsonparser
    vendor/github.com/buger/goterm
    vendor/github.com/BurntSushi/toml
    vendor/github.com/cenkalti/backoff
    vendor/github.com/cespare/xxhash
    vendor/github.com/cilium/ebpf
    vendor/github.com/containerd/containerd
    vendor/github.com/hashicorp/go-cleanhttp
    vendor/github.com/hashicorp/golang-lru
    vendor/github.com/cloudflare/circl
    vendor/github.com/containers/image
    vendor/github.com/containers/common
    vendor/github.com/containerd/cgroups
    vendor/github.com/containerd/console
    vendor/github.com/containerd/continuity
    vendor/github.com/containerd/errdefs
    vendor/github.com/containerd/platforms
    vendor/github.com/containerd/fifo
    vendor/github.com/containerd/go-cni
    vendor/github.com/containerd/go-runc
    vendor/github.com/containerd/stargz-snapshotter
    vendor/github.com/containerd/ttrpc
    vendor/github.com/containernetworking/cni
    vendor/github.com/containernetworking/plugins
    vendor/github.com/containers/storage
    ~~vendor/github.com/container-orchestrated-devices/container-device-interface
    vendor/github.com/coreos/go-iptables
    vendor/github.com/coreos/go-systemd
    vendor/github.com/go-log/log
    vendor/github.com/cpuguy83/go-md2man
    vendor/github.com/creack/pty
    vendor/github.com/cyphar/filepath-securejoin
    vendor/github.com/d2g/dhcp4
    vendor/github.com/d2g/dhcp4client
    vendor/github.com/docker/distribution
    ~~vendor/github.com/docker/docker
    vendor/github.com/docker/docker-credential-helpers
    vendor/github.com/docker/go-connections
    vendor/github.com/docker/go-metrics
    vendor/github.com/docker/go-units
    vendor/github.com/docker/libtrust
    vendor/go.etcd.io/bbolt
    vendor/go.opencensus.io
    vendor/github.com/fatih/color
    vendor/github.com/felixge/httpsnoop
    vendor/github.com/fsnotify/fsnotify
    vendor/github.com/garyburd/redigo
    vendor/github.com/godbus/dbus
    vendor/github.com/gogo/protobuf
   ~~vendor/github.com/golang/protobuf
    vendor/github.com/golang/groupcache
   ~vendor/github.com/go-log/log
    vendor/github.com/go-logr/logr
    vendor/github.com/go-logr/stdr
    vendor/github.com/gofrs/flock
    vendor/github.com/gogo/googleapis
    vendor/github.com/google/go-cmp
    vendor/github.com/google/shlex
    vendor/github.com/google/uuid
    vendor/github.com/gorilla/handlers
    vendor/github.com/gorilla/mux
    vendor/github.com/gorilla/websocket
    vendor/gotest.tools/v3
    vendor/github.com/hashicorp/errwrap
    vendor/github.com/hashicorp/go-multierror
    vendor/github.com/inconshreveable/mousetrap
    ~vendor/github.com/j-keck/arping
    vendor/github.com/klauspost/compress
    vendor/github.com/klauspost/pgzip
    vendor/github.com/mattn/go-colorable
    vendor/github.com/mattn/go-isatty
    vendor/github.com/mattn/go-runewidth
    vendor/github.com/mattn/go-shellwords
    vendor/github.com/Microsoft/go-winio
    vendor/github.com/Microsoft/hcsshim
    vendor/github.com/mitchellh/go-homedir
    vendor/github.com/moby/docker-image-spec
    vendor/github.com/moby/patternmatcher
    vendor/github.com/moby/locker
    vendor/github.com/moby/term
  ~~vendor/github.com/moby/sys
    vendor/github.com/morikuni/aec
    vendor/github.com/Netflix/go-expect
    vendor/github.com/networkplumbing/go-nft
    vendor/github.com/opencontainers/go-digest
    vendor/github.com/opencontainers/image-spec
    vendor/github.com/opencontainers/runc
    vendor/github.com/opencontainers/runtime-spec
    vendor/github.com/opencontainers/runtime-tools
    vendor/github.com/opencontainers/selinux
    vendor/github.com/opencontainers/umoci
    vendor/github.com/pelletier/go-toml
    vendor/github.com/pkg/errors
    vendor/github.com/prometheus/client_golang
    vendor/github.com/prometheus/client_model
    vendor/github.com/prometheus/common
    vendor/github.com/prometheus/procfs
    vendor/github.com/ProtonMail/go-crypto
    vendor/github.com/rivo/uniseg
    vendor/github.com/rootless-containers/proto
    vendor/github.com/russross/blackfriday
    vendor/github.com/safchain/ethtool
    vendor/github.com/samber/lo
    vendor/github.com/seccomp/libseccomp-golang
    vendor/sigs.k8s.io/yaml
    vendor/github.com/sirupsen/logrus
    vendor/github.com/spf13/cobra
    vendor/github.com/spf13/pflag
    vendor/github.com/sylabs/json-resp
   ~vendor/github.com/sylabs/scs-build-client
   ~vendor/github.com/sylabs/scs-key-client
   ~vendor/github.com/sylabs/scs-library-client
    vendor/github.com/syndtr/gocapability
    vendor/github.com/tonistiigi/fsutil
    vendor/github.com/tonistiigi/units
    vendor/github.com/vbatts/go-mtree
    vendor/github.com/vbatts/tar-split
    vendor/github.com/vbauerster/mpb
    vendor/github.com/vishvananda/netlink
    vendor/github.com/vishvananda/netns
    vendor/github.com/VividCortex/ewma
    vendor/github.com/xeipuuv/gojsonpointer
    vendor/github.com/xeipuuv/gojsonreference
    vendor/github.com/xeipuuv/gojsonschema
    vendor/github.com/go-jose/go-jose/v4
  ~~vendor/golang.org/x/crypto::PATCHED
    vendor/golang.org/x/exp
    vendor/golang.org/x/mod
    vendor/golang.org/x/net
    vendor/golang.org/x/sync
  ~~vendor/golang.org/x/sys
    vendor/golang.org/x/term
    vendor/golang.org/x/text
    vendor/gopkg.in/yaml.v2
    vendor/gopkg.in/yaml.v3
  ~~vendor/gotest.tools
  ~~vendor/google.golang.org/genproto
  ~~vendor/google.golang.org/grpc
  ~~vendor/google.golang.org/protobuf
    vendor/mvdan.cc/sh
Comment:
  "~"  : not packaged;
  "~~" : FTBFS with system package;

Files: *
Copyright: 2015-2017, Gregory M. Kurtzer <gmkurtzer@lbl.gov>
           2016-2017, The Regents of the University of California
           2017, SingularityWare, LLC
           2017-2019, SyLabs, Inc.
           2020, Control Command Inc.
License: BSD-3-Clause-LBNL and BSD-3-Clause
Comment:
 Newer parts of the code base are under the standard BSD-3-Clause license.

Files: mconfig
       makeit/*
       mlocal/checks/basechecks.chk
Copyright: 2016-2018 Yannick Cote <yhcote@gmail.com>
License: ISC

Files: internal/pkg/runtime/engine/config/oci/generate/generate.go
Copyright: 2015 The Linux Foundation
License: Apache-2.0

Files: cmd/internal/cli/instance_stats_linux.go
Copyright: 2022, Vanessa Sochat
License: BSD-3-Clause

Files: vendor/golang.org/x/crypto/*
Copyright: 2009-2013, The Go Authors
                      Fan Jiang <fanjiang@thoughtworks.com>
                      Sofia Celi <sceli@thoughtworks.com>
License: BSD-3-Clause-Google
Comment:
 This is modified from the original in (at least)
 vendor/golang.org/x/crypto/openpgp/packet/private_key.go

Files: vendor/golang.org/x/sys/*
       vendor/golang.org/x/time/*
Copyright: 2009 The Go Authors
License: BSD-3-Clause

Files: vendor/github.com/AdamKorcz/go-fuzz-headers/*
Copyright: AdamKorcz
License: Apache-2.0
Comment: As per https://github.com/AdaLogics/go-fuzz-headers/blob/main/LICENSE

Files: vendor/github.com/agext/levenshtein/*
Copyright: Alex Bucataru <alex@alrux.com>
License: Apache-2.0

Files: vendor/github.com/anchore/go-struct-converter/*
Copyright: Anchore, Inc.
License: Apache-2.0

Files: vendor/github.com/armon/circbuf/*
Copyright: 2013 Armon Dadgar
License: Expat

Files: vendor/github.com/astromechza/etcpwdparse/*
Copyright: 2017 AstromechZA
License: Expat

Files: vendor/github.com/adigunhammedolalekan/registry-auth/*
Copyright: 2020 Lekan Adigun
License: Expat

Files: vendor/github.com/docker/*
Copyright: 2014 Docker, Inc
License: Apache-2.0

Files: vendor/github.com/containerd/nydus-snapshotter/*
Copyright: The containerd Authors
License: Apache-2.0

Files: vendor/github.com/containerd/typeurl/v2/*
Copyright: The containerd Authors
License: Apache-2.0

Files: vendor/tags.cncf.io/container-device-interface/*
Copyright: 2021-2023 The CDI Authors
License: Apache-2.0

Files: vendor/github.com/distribution/reference/*
Copyright: CNCF Maintainers <cncf-distribution-maintainers@lists.cncf.io>
License: Apache-2.0

Files: vendor/github.com/gosimple/slug/*
Copyright: 2016-2021 Dobrosław Żybort
License: MPL-2.0

Files: vendor/github.com/gosimple/unidecode/*
Copyright: 2014 Rainy Cape S.L. <hello@rainycape.com>
License: Apache-2.0

Files: vendor/github.com/hashicorp/go-immutable-radix/v2/*
Copyright: 2015 HashiCorp, Inc.
License: MPL-2.0

Files: vendor/github.com/secure-systems-lab/go-securesystemslib/*
Copyright: 2021 NYU Secure Systems Lab
License: Expat

Files: vendor/github.com/shibumi/go-pathspec/*
Copyright: 2014, Sander van Harmelen
           2020, Christian Rebischke
License: Apache-2.0

Files: vendor/github.com/moby/sys/*
Copyright: 2020 Kir Kolyshkin <kolyshkin@gmail.com>
License: Apache-2.0

Files: vendor/github.com/shopspring/*
Copyright: 2015 Spring, Inc.
License: Expat

Files: vendor/github.com/google/go-containerregistry/*
Copyright: Google
License: Apache-2.0

Files: vendor/github.com/grpc-ecosystem/grpc-gateway/*
Copyright: 2015, Gengo, Inc.
 2009 The Go Authors
License: BSD-3-Clause

Files: vendor/github.com/in-toto/in-toto-golang/*
Copyright: 2018 New York University
License: Apache-2.0

Files: vendor/github.com/mitchellh/hashstructure/*
Copyright: 2016 Mitchell Hashimoto
License: Expat

Files: vendor/github.com/moby/buildkit/*
Copyright: Docker Inc.
License: Expat

Files: vendor/github.com/letsencrypt/boulder/*
Copyright: Let's Encrypt (ISRG)
License: MPL-2.0

Files: vendor/github.com/package-url/packageurl-go/*
Copyright: the purl authors
License: Expat

Files: vendor/github.com/sigstore/sigstore/*
Copyright: 2021 Sigstore Authors
License: Apache-2.0

Files: vendor/github.com/spdx/tools-golang/*
Copyright: Steve Winslow <steve@swinslow.net>
           Rishabh Bhatnagar <bhatnagarrishabh4@gmail.com>
           Brandon Lum <lumjjb@gmail.com>
License: Apache-2.0 or GPL-2+
Comment: docs are excluded so skipped the license for docs

Files: vendor/github.com/sylabs/oci-tools/*
Copyright: 2023 Sylabs Inc.
License: Apache-2.0

Files: vendor/github.com/titanous/rocacheck/*
Copyright: 2017, Jonathan Rudenberg
           2017, CRoCS, EnigmaBridge Ltd.
License: Expat

Files: vendor/github.com/tonistiigi/go-archvariant/*
       vendor/github.com/tonistiigi/vt100/*
       vendor/github.com/tonistiigi/go-csvvalue/*
Copyright: 2022 Tõnis Tiigi
License: Expat

Files: vendor/go.opentelemetry.io/otel/*
       vendor/go.opentelemetry.io/contrib/*
       vendor/go.opentelemetry.io/proto/*
Copyright: OpenTelemetry - CNCF
License: Apache-2.0

Files: vendor/google.golang.org/grpc/*
Copyright: 2014-2019 Google Inc.
           2014-2020 gRPC authors
License: Apache-2.0

Files: vendor/google.golang.org/genproto/*
Copyright: 2016-2017 Google Inc.
License: Apache-2.0

Files: vendor/google.golang.org/protobuf/*
Copyright: 2018-2020 The Go Authors
License: BSD-3-Clause

Files: vendor/github.com/golang/protobuf/*
Copyright: 2008-2016, The Go Authors.
           2008 Google Inc.
License: BSD-3-Clause

Files: debian/*
Copyright: 2016,      Dave Love <fx@gnu.org>
           2016-2017, Yaroslav Halchenko <debian@onerussian.com>
           2019,      Afif Elghraoui <afif@debian.org>
           2022-2023  Nilesh Patra <nilesh@debian.org>
License: BSD-2-Clause

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 https://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
 contributors may be used to endorse or promote products derived from this
 software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-Clause-LBNL
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 (1) Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 .
 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 (3) Neither the name of the University of California, Lawrence Berkeley
 National Laboratory, U.S. Dept. of Energy nor the names of its contributors
 may be used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 You are under no obligation whatsoever to provide any bug fixes, patches, or
 upgrades to the features, functionality or performance of the source code
 ("Enhancements") to anyone; however, if you choose to make your Enhancements
 available either publicly, or directly to Lawrence Berkeley National
 Laboratory, without imposing a separate written license agreement for such
 Enhancements, then you hereby grant the following license: a  non-exclusive,
 royalty-free perpetual license to install, use, modify, prepare derivative
 works, incorporate into other computer software, distribute, and sublicense
 such enhancements or derivative works thereof, in binary and source code form.
 .
 This software is licensed under a customized 3-clause BSD license.  Please
 consult LICENSE file distributed with the sources of this project regarding
 your rights to use or distribute this software.
 .
 NOTICE.  This Software was developed under funding from the U.S. Department of
 Energy and the U.S. Government consequently retains certain rights. As such,
 the U.S. Government has been granted for itself and others acting on its
 behalf a paid-up, nonexclusive, irrevocable, worldwide license in the Software
 to reproduce, distribute copies to the public, prepare derivative works, and
 perform publicly and display publicly, and to permit other to do so.

License: BSD-3-Clause-Google
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following disclaimer
   in the documentation and/or other materials provided with the
   distribution.
 * Neither the name of Google Inc. nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: MPL-2.0
 On Debian systems you can finde the full text of the Mozilla Public License
 version 2.0 at /usr/share/common-licenses/MPL-2.0.

License: unlicense.org
 This is free and unencumbered software released into the public domain.
 .
 Anyone is free to copy, modify, publish, use, compile, sell, or
 distribute this software, either in source code form or as a compiled
 binary, for any purpose, commercial or non-commercial, and by any means.
 .
 In jurisdictions that recognize copyright laws, the author or authors
 of this software dedicate any and all copyright interest in the software
 to the public domain. We make this dedication for the benefit of the
 public at large and to the detriment of our heirs and successors. We
 intend this dedication to be an overt act of relinquishment in
 perpetuity of all present and future rights to this software under
 copyright law.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 On Debian systems the full text of the GPL-2 can be found in
 /usr/share/common-licenses/GPL-2
